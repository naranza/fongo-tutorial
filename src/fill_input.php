<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'previous_amount' => [],
  'amount' => [
    'fill_source' => 'input',
    'fill_value' => 'previous_amount',
  ]
];

/* simulate user input */
$input = [
  'previous_amount' => 100
];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
var_dump($result->processed);
