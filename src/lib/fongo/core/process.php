<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/core/path.php';
require_once FONGO_DIR . '/read/dict.php';
require_once FONGO_DIR . '/read/file.php';
require_once FONGO_DIR . '/read/option.php';
require_once FONGO_DIR . '/struct/term.php';
require_once FONGO_DIR . '/struct/result.php';
require_once FONGO_DIR . '/process/garbage.php';
require_once FONGO_DIR . '/chain/filter.php';
require_once FONGO_DIR . '/chain/validate.php';

function fongo_process(array $dict, array $input, array $context = []): fongo_result
{

  $result = new fongo_result();

  list($result->processed, $result->garbage) = fongo_process_garbage($dict, $input);

  /* composed list of fields ordered by processing */
  $ordered = [];
  $after = [];
  foreach ($dict as $field => $term) {
    if (is_array($term)) {
      $dict[$field] = new fongo_term($term);
    }
    if ($dict[$field] instanceof fongo_term) {
      if ('' !== $dict[$field]->compose) {
        $after[$field] = true;
      } else {
        $ordered[$field] = true;
      }
    } else {
      $result->errors[$field] = 'Invalid dictionary term definition';
    }
  }
  $ordered += $after;

  /* process ordered list */
  foreach (array_keys($ordered) as $field) {
    $field_missed = !array_key_exists($field, $result->processed);

    /* compose term */
    if ('' !== $dict[$field]->compose) {
      $result->processed[$field] = $dict[$field]->compose;
      $fields = [];
      preg_match_all('/{+(.*?)}/', $dict[$field]->compose, $fields);
      foreach ($fields[1] ?? [] as $c_field) {
        $result->processed[$field] = str_replace(
          '{' . $c_field . '}',
          (array_key_exists($c_field, $result->processed) ? (string) $result->processed[$c_field] : ''),
          $result->processed[$field]);
      }
      $field_missed = false;
    }

    /* fill if empty */
    $filled = false;
    switch ($dict[$field]->fill_source) {
      case 'input':
        if (array_key_exists($dict[$field]->fill_value, $input)) {
          $value_to_fill = $input[$dict[$field]->fill_value];
          $filled = true;
        }
        break;
      case 'context':
        if (array_key_exists($dict[$field]->fill_value, $context)) {
          $value_to_fill = $context[$dict[$field]->fill_value];
          $filled = true;
        }
        break;
      case 'default':
        $value_to_fill = $dict[$field]->default;
        $filled = true;
        break;
      case 'value':
        $value_to_fill = $dict[$field]->fill_value;
        $filled = true;
        break;
    }
    if ($filled) {
      $result->processed[$field] = $value_to_fill;
      $field_missed = false;
    }

    /* missed management */
    if ($field_missed && $dict[$field]->required) {
      $result->missed[$field] = true;
      $result->errors[$field] = $dict[$field]->missing_message;
    } else if (!$field_missed) {
      /* check if empty_allowed and valid empty_function function */
      if ($dict[$field]->empty_allowed && is_callable($dict[$field]->empty_function['function'] ?? '')) {
        $field_is_empty = $dict[$field]->empty_function['function'](
          $result->processed[$field],
          $dict[$field]->empty_function['args'] ?? []);
      } else {
        $field_is_empty = false;
      }
      if ($field_is_empty) {
        $result->empties[$field] = true;
      } else {
        /* filter field */
        if (!empty($dict[$field]->filters)) {
          foreach ($dict[$field]->filters as $idx => $filter) {
            if (is_string($filter)) {
              $dict[$field]->filters[$idx] = fongo_read_file($filter);
            }
          }
          list($value_filtered, $error) = fongo_chain_filter($dict[$field]->filters, $result->processed[$field]);
          if ('' === $error) {
            $result->processed[$field] = $value_filtered;
            $result->filtered[$field] = true;
          }
        }

        /* validation */
        if (!empty($dict[$field]->rules)) {
          foreach ($dict[$field]->rules as $idx => $rule) {
            if (is_string($rule)) {
              $dict[$field]->rules[$idx] = fongo_read_file($rule);
            }
          }
          list($message, $code) = fongo_chain_validate(
            $dict[$field]->rules, // rules
            $result->processed[$field], // value,
            ['input' => $input, 'context' => $context, 'options' => fongo_read_option($dict[$field]->options)] // context
          );
          $result->validated[$field] = true;
          if ('' !== $message) {
            $result->errors[$field] = $message;
            $result->codes[$field] = $code;
          }
        }
      }
    }
  }

  return $result;
}
