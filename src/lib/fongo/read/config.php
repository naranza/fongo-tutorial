<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function fongo_read_config(string $path): array
{
  if (!is_file($path) || !is_readable($path)) {
    throw new exception(sprintf("%s not file or readable", $path));
  }
  $result = include($path);
  if (!is_array($result)) {
    if (isset($config) && is_array($config)) {
      $result = $config;
    } else {
      $result = [];
    }
  }
  return $result;
}
