<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

final class fongo_result
{

  public array $processed = [];
  public array $garbage = [];
  public array $errors = [];
  public array $missed = [];
  public array $filtered = [];
  public array $validated = [];
  public array $empties = [];
  public array $codes = [];

  public function __get(string $name): mixed
  {
    throw new exception("($name) __get not allowed on fongo_result");
  }

  public function __set($name, $value)
  {
    throw new exception('__set not allowed on fongo_result');
  }

  public function __construct(array $config = [])
  {
    foreach ($config as $key => $value) {
      if (isset($this->$key)) {
        $this->$key = $value;
      }
    }
  }
}
