<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'first_name' => [],
  'middle_name' => [
    'empty_allowed' => true,
    'empty_function' => [
      'function' => function (string $value) {
        return 'none' === $value;
      }
    ]
  ],
  'last_name' => []
];

/* simulate user input */
$input = [
  'first_name' => 'john',
  'middle_name' => 'none',
  'last_name' => 'smith',
];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
var_dump($result->processed);
