<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'first_name' => [
    'rules' => [
      ['function' => function (string $value) {
          return '' !== $value;
        }]
    ]
  ],
  'last_name' => [
    'rules' => [
      ['function' => function (string $value) {
          return '' !== $value;
        }],
      ['function' => function (string $value) {
          return strlen($value) >= 5;
        }],
    ]
  ]
];

/* simulate user input */
$input = [
  'first_name' => 'john',
  'last_name' => 'smith',
];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
var_dump($result->processed);
