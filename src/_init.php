<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

/* include Fongo library */
include __DIR__ . '/lib/fongo/initme.php';
require_once FONGO_DIR . '/core/process.php';
//require_once FONGO_DIR . '/core/path.php';
//require_once FONGO_DIR . '/read/dict.php';


/* set data path */
fongo_path('data', __DIR__ . '/data');