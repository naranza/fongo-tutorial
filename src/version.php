<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

echo "\nCiao, welcome to Fongo tutorial.\n\n";
echo sprintf("Fongo version: %s\n", FONGO_VERSION);
echo sprintf("Codename: %s\n", FONGO_CODENAME);
echo "\n";

