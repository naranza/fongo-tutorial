<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'first_name' => [
    'required' => false
  ],
  'last_name' => [],
];


/* simulate user input */
$input = [
  'last_name' => 'Smith',
];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
