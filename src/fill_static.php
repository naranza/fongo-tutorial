<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'amount' => [
    'fill_source' => 'value',
    'fill_value' => 100,
    ]
];

/* simulate user input */
$input = [];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
var_dump($result->processed);
