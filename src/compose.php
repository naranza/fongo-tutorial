<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include '_init.php';

/* define dictionary */
$dict = [
  'dob_year' => [],
  'dob_date' => [
    'compose' => '{dob_year}-{dob_month}-{dob_day}',
  ],
  'dob_month' => [],
  'dob_day' => [],
];

/* simulate user input */
$input = [
  'dob_year' => 2000,
  'dob_day' => 31,
  'dob_month' => 12,
];

/* process the user input against the dictionary */
$result = fongo_process($dict, $input);

print_r($result);
var_dump($result->processed);
